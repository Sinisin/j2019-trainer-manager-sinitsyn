package by.itstep.collection.manager.util;

import javax.persistence.*;

public class EntityManagerUtil {

    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("trainer-manager-itstep");

    public static EntityManager getEntityManager(){
        EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
        return em;
    }
}
