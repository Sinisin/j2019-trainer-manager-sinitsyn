package by.itstep.collection.manager.entity;

public enum AdminRole {

    ADMIN,
    SUPER_ADMIN

}
