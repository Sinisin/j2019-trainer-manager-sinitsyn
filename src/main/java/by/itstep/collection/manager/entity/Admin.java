package by.itstep.collection.manager.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "`admin`")
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "`name`")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "`email`")
    private String email;

    @Column(name = "admin_role")
    private AdminRole adminRole;

    @Column(name = "`password`")
    private String password;

}
