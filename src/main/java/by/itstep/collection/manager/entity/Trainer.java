package by.itstep.collection.manager.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "trainer")
public class Trainer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "`name`")
    private String name;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "work_experience")
    private int workExperience;

    @Column(name = "achievements")
    private String achievements;

    @Column(name = "`avatar`")
    private String avatar;

    @Column(name = "`email`")
    private String email;

    @Column(name = "`password`")
    private String password;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "trainer")
    private List<Comment> usersComments;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "trainer")
    private List<Appointment> appointments;



}
